# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libkscreen
pkgver=5.20.0
pkgrel=0
pkgdesc="KDE screen management software"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://www.kde.org/workspaces/plasmadesktop/"
license="LGPL-2.1-or-later AND GPL-2.0-or-later AND (GPL-2.0-only OR GPL-3.0-only)"
depends_dev="qt5-qtbase-dev qt5-qtx11extras-dev kwayland-dev"
makedepends="$depends_dev extra-cmake-modules doxygen graphviz qt5-qttools-dev"
source="https://download.kde.org/stable/plasma/$pkgver/libkscreen-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"
options="!check" # Fails due to requiring dbus-x11 and it running

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}
sha512sums="d516cff6572abe02406451e71efc19749c42eb719f8506df492a462af1a3436ac16f4546ae974328572e0c169cc2a74e7bf055c5ab3c0de13729f9f254506ec1  libkscreen-5.20.0.tar.xz"
